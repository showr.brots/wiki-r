\documentclass[
	% -- opções da classe memoir --
	12pt,				% tamanho da fonte
	openright,			% capítulos começam em pág ímpar (insere página vazia caso preciso)
	oneside,			% para impressão em recto e verso. Oposto a oneside
	a4paper,			% tamanho do papel. 
	% -- opções da classe abntex2 --
	%chapter=TITLE,		% títulos de capítulos convertidos em letras maiúsculas
	%section=TITLE,		% títulos de seções convertidos em letras maiúsculas
	%subsection=TITLE,	% títulos de subseções convertidos em letras maiúsculas
	%subsubsection=TITLE,% títulos de subsubseções convertidos em letras maiúsculas
	% -- opções do pacote babel --
	english,			% idioma adicional para hifenização
	french,				% idioma adicional para hifenização
	spanish,			% idioma adicional para hifenização
	brazil,				% o último idioma é o principal do documento
	]{abntex2}

% ---
% PACOTES
% ---

% ---
% Pacotes fundamentais 
% ---
\usepackage{lmodern}			    % Usa a fonte Latin Modern
\usepackage[T1]{fontenc}		  % Selecao de codigos de fonte.
\usepackage[utf8]{inputenc}		% Codificacao do documento (conversão automática dos acentos)
\usepackage{indentfirst}   		% Indenta o primeiro parágrafo de cada seção.
\usepackage{nomencl}  		  	% Lista de simbolos
\usepackage{color}				    % Controle das cores
\usepackage{graphicx}			    % Inclusão de gráficos
\usepackage{microtype} 			  % para melhorias de justificação
\usepackage[alf,abnt-etal-cite=1]{abntex2cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{float}
\usepackage[brazilian,hyperpageref]{backref}	 % Paginas com as citações na bibl
\usepackage{everypage}
% ---

% ---
% Pacotes de citações
% ---
\usepackage[brazilian,hyperpageref]{backref}	 % Paginas com as citações na bibl
\usepackage[alf]{abntex2cite}	% Citações padrão ABNT

% ---
% Informações de dados para CAPA e FOLHA DE ROSTO
% ---
\titulo{Wiki-R: aprender, preservar, ensinar}
\autor{João B. G. Brito}
\local{Porto Alegre, RS}
\data{\today}
\instituicao{%
  Universidade do Brasil -- UBr
  \par
  Faculdade de Arquitetura da Informação
  \par
  Programa de Pós-Graduação}

\instituicao{%
	Universidade Federal do Rio Grande do Sul -- UFRGS
	\par
	Departamento de Engenharia de Produção e Transportes
	\par
	Programa de Pós-Graduação}

\tipotrabalho{Projeto de Pesquisa}
% O preambulo deve conter o tipo do trabalho, o objetivo, 
% o nome da instituição e a área de concentração 
\preambulo{Plataforma Wiki para aprender, preservar e disseminar conhecimentos sobre aplicações da linguagem de programação R.}
% ---

% ---
% Configurações de aparência do PDF final

% alterando o aspecto da cor azul
\definecolor{blue}{RGB}{41,5,195}

% informações do PDF
\makeatletter
\hypersetup{
     	%pagebackref=true,
		pdftitle={\@title}, 
		pdfauthor={\@author},
    	pdfsubject={\imprimirpreambulo},
	    pdfcreator={LaTeX with abnTeX2},
		pdfkeywords={abnt}{latex}{abntex}{abntex2}{projeto de pesquisa}, 
		colorlinks=true,       		% false: boxed links; true: colored links
    	linkcolor=blue,          	% color of internal links
    	citecolor=blue,        		% color of links to bibliography
    	filecolor=magenta,      		% color of file links
		urlcolor=blue,
		bookmarksdepth=4
}
\makeatother
% --- 

% --- 
% Espaçamentos entre linhas e parágrafos 
% --- 

% O tamanho do parágrafo é dado por:
\setlength{\parindent}{1.3cm}

% Controle do espaçamento entre um parágrafo e outro:
\setlength{\parskip}{0.2cm}  % tente também \onelineskip

% ---
% compila o indice
% ---
\makeindex
% ---

% ----
% Início do documento
% ----
\begin{document}

% Seleciona o idioma do documento (conforme pacotes do babel)
%\selectlanguage{english}
\selectlanguage{brazil}

% Retira espaço extra obsoleto entre as frases.
\frenchspacing 

% ----------------------------------------------------------
% ELEMENTOS PRÉ-TEXTUAIS
% ----------------------------------------------------------
% \pretextual

% ---
% Capa
% ---
\imprimircapa
% ---

% ---
% Folha de rosto
% ---
\imprimirfolhaderosto
% ---

% ---
% inserir o sumario
% ---
\pdfbookmark[0]{\contentsname}{toc}
\tableofcontents*
% ---


% ----------------------------------------------------------
% ELEMENTOS TEXTUAIS
% ----------------------------------------------------------
\textual

% ----------------------------------------------------------
% Introdução
% ----------------------------------------------------------
\chapter{INTRODUÇÃO}

Não é necessariamente desinteresse ou falta de foco. Observa-se nos estudantes a dificuldade em coordenar a relação entre atenção e recompensa --- aspecto fundamental para gerir organização, planejamento, memória, autocontrole e para atingir um objetivo \cite{Bassett.2017}. O modelo tradicional de educação pressupõe que os estudantes absorvam conhecimento de forma passiva: quietos, comportados e focados no professor  \cite{Derek.2008}. Esse formato ortodoxo colide diretamente com a dinâmica de aprendizagem que a tecnologia proporciona \cite{Zhao.2011}, da mesma forma que não acompanha a atual mecânica de interações da ambiência escolar \cite{Mikum.2017}.

Na escola, o ensino tradicional não imprime uma gratificação estimulante que abarque o ambiente social e corporativo; a nota funciona como uma moeda de troca: o que necessariamente não estimula a aprendizagem ou atraia a concentração do aluno \cite[p.~113]{Barros.2000}. A dificuldade de atenção está marcada pelo excesso de estímulos disponíveis, por isso é natural esperar que atividades com recompensas imediatas distraiam os discentes \cite{Daschmann.2014}. Jovens, na atualidade, tendem a não ouvir tudo que está sendo ensinado, não se sentem estimulados pelos exercícios em aula ou extraclasse e se entediam facilmente \cite{Nett.2010}. Dividem o foco entre a aula e uma enorme diversidade de possibilidades alçadas pelos computadores e \textit{smartphones}.

Com tanto acesso à informação tem-se distração constante. Para \citeonline{Carr.2011} as pessoas t\^em se tornando cada vez mais rasas de conhecimento: somente um pouco de atenção é dedicado para cada coisa. Por mais que os acadêmicos tenham a ilusão de que possam gerenciar múltiplas tarefas, para \citeonline{Sanbonmatsu.2013} o ser humano não consegue conduzir várias atividades ao mesmo tempo com eficiência. Como defendem \citeonline{Markman.2016}, a maneira mais competente de executar uma ação é realizar somente ela. Por esses fatos, excesso de acessibilidade à informação poderia ser visto como um problema.

\citeonline{Asimov.1988} prospectava que a internet mudaria a maneira com a qual as pessoas disseminam e buscam informação; que haveria uma época em que as pessoas não precisariam parar de aprender quando saem da escola. Atualmente, nunca foi tão fácil responder perguntas e encontrar respostas \cite{Siegel.2013}. No decorrer de uma aula, alunos verificam em tempo real o discurso dos professores, questionando e conferindo ressalvas \cite{Santos.2014}. Computadores e \textit{smartphones} interferem na aprendizagem porque muitos dos ambientes de ensino ainda não os aplicam como apoio pedagógico. Esses mecanismos são subutilizados, ignorando-se o fato dos mesmos serem uma extensão do aluno \cite{Anshari.2017}. 

Como descrevem \citeonline{Hilbert.2011}, a partir de 2003, a humanidade passou a produzir mais dados armazenados em formato digital do que em meios analógicos. Com isso, nos dias de hoje, deixa-se de decorar textos, datas ou qualquer outro dado que possa ser acessado em tempo real. Para \citeonline{Cutajar.2017}, a facilidade de acesso à informação vai além: com todas as conexões que a internet possibilita, estudantes possuem hoje uma consciência global sobre o mundo. Segundo \citeonline{Cairns.2017}, os docentes não podem mais atuar somente como um transmissores de conhecimento, fenômenos como redes sociais, disponibilidade de informação e acessibilidade à internet não podem ser ignorados. O papel do professor está em orientar o processo social de aprendizagem --- inspirar, desafiar, incitar os estudantes para que queiram aprender \cite{Zakharov.2017}. 

A motivação é a força motriz da aprendizagem: sem interesse, nenhum conteúdo é absorvido. Para \citeonline{Ariely.2009}, as pessoas vivem sobre a regra de dois mundos: um regido pelas normas sociais (voluntariado); outro pelas normas de mercado (no qual as interações são normalmente resolvidas com dinheiro). Assim, verifica-se que indivíduos  contribuem mais em uma tarefa quando a fazem por um  bem coletivo 
\citeonline{Heyman.2004}. Isso ocorre porque, independente da idade ou causa, as pessoas possuem um senso de justiça muito forte para agir em grupo  \cite{Lieberman.2013}.

Como observado no experimento de \citeonline{Rilling.2002}, em geral, parte do que faz as pessoas se sentirem bem é colaborar, contribuindo para uma causa. Esse aspecto explica o sucesso de softwares publicados sobre licença pública --- GNU GPL\footnote{GNU General Public License (Licença Pública Geral), GNU GPL ou simplesmente GPL, é a designação da licença para software livre idealizada por Richard Matthew Stallman em 1989, no âmbito do projeto GNU da Free Software Foundation (FSF) \cite[p.~7905]{Khosrow.2017}.} e, da mesma maneira, esclarece como enciclopédias virtuais, e.g. Wikipédia, edificam acurácia superior às enciclopédias clássicas, como a Britannica \cite{Giles.2005}. 

\citeonline{Smith.2017} sustenta que os melhores resultados somente ocorrem quando todos os membros de um grupo fazem o melhor para sí próprios. Contudo, grande parte da evolução humana pode ser atribuída ao altruísmo recíproco \citeonline{Trivers.1971}, um conceito que enlaça com a proposta de \citeonline{Nash.1950,Nash.1951}, no qual os melhores resultados são obtidos quando as pessoas fazem o que é melhor para sí e também para o grupo. A dinâmica promovida pelas tecnologias virtuais facilita a composição de grupos para produção de conteúdo; com isso, grupos são capazes de gerar e gerir resultados melhores do que os membros individualmente --- mesmo especialistas \cite{Surowiecki.2005}. Essa noção possui lastro nos preceitos de Inteligência Coletiva e Engenharia Social.

Para \citeonline{Well.2016}, a noção de inteligência compartilhada surge da premissa de que a colaboração de vários indivíduos, em suas diversidades de conhecimento, fomenta resultados superiores tanto para os membros quanto para o grupo. Esse modelo foi batizado por \citeonline{Johnson.1998} como Inteligência Simbiótica e por \citeonline{Surowiecki.2005} como Inteligência Coletiva. Sob essa ótica, entrelaçam-se atributos cognitivos, sociais e ambientais de colaboração abarcados pela Engenharia Social \cite{Harvey.2000}.

\citeonline[p.~5--6]{Beardsley.2014}  relata que, normalmente, a Engenharia Social recebe associações negativas por ter sido vinculada à manipulação política, religiosa e, por vezes, definida como estratégias para ataques cibernéticos (e.g. \citeonline{Mitnick.2017}). Apesar disso, na precípua, o termo foi introduzido por J.C. Van Marken, em 1894, para descrever mecanismos de engendramento colaborativo entre indivíduos que buscam um objetivo em comum \cite[p.~486]{Mandal.2007}. Nesse contexto, a tecnologia da informação tem  promovido avanços que possibilitam a formação de grupos nos quais os indivíduos compartilham informações. Na internet, Fóruns, Wiki’s, Blog’s e Vídeos são exemplos de Engenharia Social aplicados na geração, preservação e disseminação da informação. Assim, o papel da escola ganha preponderância, pois existe uma grande diferença entre informação e formação.

A formação acontece na relação dialética entre o sujeito e a sociedade que o cerca --- o aluno modifica o ambiente e o ambiente modifica o aluno \cite{Vygotsky.2014}. Essa relação não é passível de muita generalização; para \citeonline{Vygotsky.2014} o importante é a interação que cada pessoa estabelece dentro do ambiente, a chamada experiência pessoalmente significativa. \citeonline{Culver.1998} assinala que aprender é um processo emocional e, em razão disso, somente quando o aluno inclina interesse por um assunto que o conhecimento é assimilado. Esse conceito define a Engenharia Educacional, que tem reflexo na pedagogia contemporânea.

\citeonline{Palangana.2015} relata que as abordagens de Vygotsky\footnote{Psicólogo bielo-russo Lev Vygotsky (1896--1934) foi pioneiro no conceito de educação como um processo social de aprendizagem. Essa corrente pedagógica deu origem ao pensamento socioconstrutivismo ou sociointeracionismo \cite{Veer.2014}.} não poderiam ser mais atuais. Nos bancos escolares, alunos buscam naturalmente na internet suporte para dúvidas e curiosidades --- não sendo mais necessário decorar dados  \cite{Clark.1998}. Nas pesquisas \textit{on-line} uma das fontes com mais destaque é a Wikipédia, a qual foi criada em 2001 com o objetivo de complementar o projeto Nupedia\footnote{ Enciclopédia \textit{online}, fundada em março de 2000 por Jimmy Wales e Larry Sanger, escrita por especialistas \cite{Nupedia.2017}.}. Atualmente, é uma enciclopédia \textit{on-line}, multilíngue, livre, colaborativa e escrita voluntariamente por especialistas ou pessoas comuns \cite{Wikipedia.2017a}.  Utilizando a plataforma Wiki\footnote{Sistema de produção textual colaborativo, no qual é permitida a livre e coletiva construção de texto.}, o ambiente ganhou a admissão de milhares de autores e milhões de artigos --- verbetes. A Wikipédia e o modelo Wiki têm sido estudados e aplicados em diversas abordagens.

\citeonline{Pfeil.2006} exploram as relações culturais entre França, Alemanha, Japão e Holanda por intermédio da comunicação mediada por computador (CMC) na Wikipédia; \citeonline{Greenstein.2007} analisa o impacto econômico pelo prisma de impressões das enciclopédias; \citeonline{Keim.2007} destaca a acurácia das informações publicadas nesses ambientes; \citeonline{Powell.2008} descreve o impacto da Wikipédia na sociedade; \citeonline{Halavais.2008} apuram a amplitude de temas cobertos pela plataforma; \citeonline{Liao.2009} estuda os conflitos de conceitos regionais da Wikipédia na China; \citeonline{Zhiqiang.2009} e \citeonline{Rahurkar.2010} utilizam o ambiente para interpretações computacionais semânticas; \citeonline{Norik.2010} compara a relação intercultural das práticas de quatro Wikipédias em diferentes idiomas; \citeonline{Feldstein.2011} investiga a estrutura Wiki para justificar o sucesso da enciclopédia; \citeonline{Jemielniak.2016} constata o caráter aberto, participativo e democrático da organização, que em teoria está orientado para a solidariedade sustentável; \citeonline{Adam.2016} examina o comportamento colaborativo em relação ao perfil de alunos; e \citeonline{Samuel.2017} propõe um projeto de ensino que utiliza a plataforma Wiki.

Na educação, a plataforma Wiki tem sido amplamente utilizada para compor ambientes privados de aprendizagem: \citeonline{Trocky.2016} analisam a eficácia na obtenção dos resultados de aprendizagem para alunos de enfermagem; \citeonline{Arriba.2017} apresenta uma experiência em aprendizagem colaborativa para grupos com muitos alunos; e \citeonline{Biasutti.2017} elabora um estudo comparativo entre a atuação de Fóruns virtuais e Wikis no apoio do ensino. De modo geral, no entanto, não foram identificadas práticas que utilizassem os alunos como protagonistas do conteúdo, e, o conteúdo, como uma extensão natural de conhecimento do aluno. Com base nesse cenário, introduz-se uma das habilidades mais proeminentes do mundo atual: saber programar.

Para \citeonline{Resnick.2014}, mesmo que a demanda por programadores de computador seja muito superior a oferta do mercado, esse não é a principal razão pela qual a codificação (programação)  pode ser útil na vida de todos --- assim como aconteceu com a escrita. \citeonline{Resnick.2014} defende que a habilidade de codificar um software contribui para organizar as ideias. Como na alfabetização, não aprende-se para escrever, escreve-se para aprender; e, a medida que se escreve, o conhecimento é assimilado \cite{Freire.1992}. Por esse argumento, não deve-se apenas aprender a programar, mas sim, programar para aprender. Jovens na atualidade, apresentam aparente fluência com as novas tecnologias. No entanto, na grande maioria das vezes são apenas usuários passivos: não criam, engendram, ou projetam tecnologias. Para uma participação completa, na atual sociedade, é necessário ter fluência para se expressar e compreender os mecanismos que arquitetam os softwares \cite{Resnick.2017}. Sob esse aspecto, uma das linguagens de programação que vem ganhado cada vez mais espaço na ambiência acadêmica é o R.

Criada em 1996 por Ross Ihaka e Robert Gentleman na universidade de Auckland, Nova Zelândia, R é uma linguagem de programação gratuita e de domínio público (GPL) \cite{R.2017}. Desde sua origem, tem sido constantemente aprimorada pelo esforço colaborativo de uma comunidade mundial \cite{RDocumentation.2017}. Apesar de possuir alicerces na estatística, a facilidade de manipular dados e produzir gráficos impulsionou pesquisas analíticas nas mais diversas áreas de conhecimento: análise de sobrevivência \cite{Moore.2016}, análise molecular \cite{Ortutay.2017}, ciências sociais \cite{Vinod.2009}, bioinformática \cite{Gentleman.2006}, geologia \cite{Malone.2016}, psicologia \cite{Belhekar.2016}, astrofísica \cite{Hilbe.2017}, ciências políticas \cite{Monogan.2015}, ciências humanas \cite{Arnold.2015}, literatura \cite{Jockers.2014}, controle de qualidade industrial \cite{Cano.2015}, marketing \cite{Chapman.2015}, bioestatística \cite{Shahbaba.2011}, algoritmos e programação \cite{Ergul.2013}, linguística \cite{Gries.2013}, ciências forenses \cite{Curran.2010}, inferência estatística Clássica \cite{Tattar.2016}, mídias sociais \cite{Heimann.2014}, análises de questionários \cite{Bartolucci.2015}, arqueologia \cite{Carlson.2017}, teoria da resposta \cite{Baker.2017}, educação \cite{Gonzalez.2017}, agricultura \cite{Plant.2012}, teoria do risco \cite{Kaas.2008}, saúde pública \cite{Chan.2015}, matemática \cite{Vinod.2011}, ciência do comportamento \cite{Wilcox.2011}, econometria \cite{Kleiber.2008}, geografia \cite{Brunsdon.2015}, filogenética e evolução \cite{Paradis.2011}, probabilidade \cite{Horgan.2011}, genética \cite{Gondro.2015}, história \cite{Willekens.2014}, computação em nuvem \cite{Ohri.2014}, ciências atuariais \cite{Charpentier.2015}, inferência estatística Bayesiana \cite{Bolstad.2004}, biomedicina \cite{Chen.2010}, finanças \cite{Pfaff.2016}, psicofísica \cite{Knoblauch.2012}, toxicologia \cite{Hothorn.2016}, ciência de dados \cite{Baumer.2017}, engenharia \cite{Bloomfield.2014}, esportes \cite{Marchi.2013}, desenvolvimento web \cite{Beeley.2016}, projeto de experimentos \cite{Lawson.2014}, astronomia \cite{Feigelson.2012}, análise de redes sociais \cite{Samatova.2013}, ecologia \cite{Stevens.2009}. Por conseguinte, observa-se a aderência do R nos mais notáveis centros acadêmicos do mundo.

As principais universidades de excelência mundial \cite{Qs.2017}, têm adotado o uso da linguagem R como parte integrante do currículo de ensino. Dentre as entidades mais expressivas que promovem cursos de extensão, disciplinas de graduação e pós-graduação, destacam-se: \citeonline{Harvard.2017}, \citeonline{MIT.2017}, \citeonline{Cambridge.2017}, \citeonline{Stanford.2017}, \citeonline{Oxford.2017}, \citeonline{UCL.2017}, \citeonline{ICL.2017}, \citeonline{Chicago.2017}, \citeonline{Zurich.2017}, \citeonline{Singapore.2017}, \citeonline{Yale.2017}, \citeonline{Oregon.2016}, \citeonline{Cornell.2017}, \citeonline{Hopkins.2017}, \citeonline{Columbia.2017}, \citeonline{ANU.2017}, \citeonline{KCL.2017}, \citeonline{Edinburgh.2017}, \citeonline{Kong.2017}, \citeonline{Toronto.2017}, \citeonline{Zurich.2017}, \citeonline{Munich.2017}, \citeonline{USP.2017}, \citeonline{UNICAMP.2017}. Não obstante, as maiores companhias de tecnologia \cite{Forbes.2017} têm incorporado o uso do R como ferramenta essencial para ciência de dados (\textit{Data Mining}, Inteligência Artificial, \textit{Deep Learning}, \textit{Big Data}, \textit{Machine Learning}): \citeonline{IBM.2017}, \citeonline{Oracle.2017}, \citeonline{Intel.2017} \citeonline{Amazon.2017}, \citeonline{Microsoft.2017}, \citeonline{Google.2017}, Facebook \cite{Northeastern.2017}. 

Com base no contexto apresentado, o presente trabalho propõe uma prática de ensino que utiliza a plataforma Wiki pública e aberta, denominada Wiki-R, para aprender, preservar e disseminar aplicações da linguagem R. Nesse contexto, o aluno é protagonista na aquisição e preservação do conhecimento e, o professor, orienta o processo social de aprendizagem. Na conjuntura, a Wiki-R não visa atuar como uma ferramenta de apoio pedagógico, ela é utilizada para ser uma extensão natural de conhecimento do estudante. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Justificativa}
\label{Justificativa}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Metodologia}
\label{Metodologia}

\section{Ensinar}


\section{Preservar}

\subsection{Wiki-R}

A plataforma Wiki-R irá utilizar a aplicação \citeonline{MediaWiki.2017} com servidor \citeonline{PHP.2017} e banco de dados \citeonline{MySQL.2017}. A hospedagem do site será no centro de processamento de dados da Universidade Federal do Rio Grande do Sul (UFRGS), com acesso pelo link: \href{https://www.ufrgs.br/wiki-r}{https://www.ufrgs.br/wiki-r}.

\subsubsection{Hospedagem}

\href{https://www.ufrgs.br/wiki-r}{https://www.ufrgs.br/wiki-r}

\subsubsection{Plataforma Wiki}

\subsubsection{Servidor PHP}

\subsubsection{Banco de Dados}

\subsubsection{Permissões de Acesso}

\section{Disseminar}

\subsection{SEO}


\section{Projeto de experimentos}

\subsection{Métricas}

\subsection{Análises}

\section{Teoria dos Jogos Evolutivos}

\subsection{Relação entre colaboração e aprendizagem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Cronograma}



% ----------------------------------------------------------
% Referências bibliográficas
% ----------------------------------------------------------
\bibliography{referencias}

%---------------------------------------------------------------------
% INDICE REMISSIVO
%---------------------------------------------------------------------

\phantompart

\printindex


\end{document}
